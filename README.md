<p align="center">
  <a href="https://github.com/baomidou/mybatis-plus">
   <img alt="Mybatis-Plus-Logo" src="https://raw.githubusercontent.com/baomidou/logo/master/mybatis-plus-logo-new-mini.png">
  </a>
</p>

<p align="center">
  Born To Simplify Development
</p>

<p align="center">
  <a href="https://search.maven.org/#search%7Cga%7C1%7Cg%3A%22com.baomidou%22%20AND%20a%3A%22mybatis-plus%22">
    <img alt="maven" src="https://img.shields.io/maven-central/v/com.baomidou/mybatis-plus.svg?style=flat-square">
  </a>

  <a href="https://www.apache.org/licenses/LICENSE-2.0">
    <img alt="code style" src="https://img.shields.io/badge/license-Apache%202-4EB1BA.svg?style=flat-square">
  </a>

  <a href="https://gitter.im/baomidou/mybatis-plus?utm_source=badge&utm_medium=badge&utm_campaign=pr-badge&utm_content=badge">
    <img alt="Join the chat at https://gitter.im/baomidou/mybatis-plus" src="https://badges.gitter.im/baomidou/mybatis-plus.svg">
  </a>
</p>

> 规划计划

![MP4](https://images.gitee.com/uploads/images/2022/0316/222951_c8f6f75a_12260.png "mp4.png")


[官方文档](http://baomidou.com)

[企业版 Mybatis-Mate 高级特性](https://gitee.com/baomidou/mybatis-mate-examples)

### 多表Join支持使用说明
1、准备2个po
 ```java
@Data
@TableName(value="user")
public class User {

    @TableId("user_id")
    private Integer userId;

    @TableField("name")
    private String name;

    @TableField("age")
    private Integer age;

    @TableField("sex")
    private String sex;

	//指定这个字段和School 的id字段关联，如果不是和id字段关联可以使用targetFields属性进行关联
    @TableField(value="school_id",target = School.class)
    private Integer schoolId;

    //指定关联关系，和目标类  如果是  onetoone 则不需要指定target
    @TableField(relation = Relation.ONE_TO_MANY,target = School.class)
    List<School> schools;
}

```
```java
@Data
@TableName(value="school",autoResultMap = true) //这里设置自动生成resultMap 不需要别的操作
public class School {
    @TableId
    private Integer id;

    @TableField("school_name")
    private String schoolName;

    @TableField("remark")
    private String remark;
}

```
2、启动join插件(不然返回结果映射会有问题，此插件动态修改resultMap)
```java
    @Bean
    public MybatisPlusInterceptor mybatisPlusInterceptor() {
        MybatisPlusInterceptor interceptor = new MybatisPlusInterceptor();
        interceptor.addInnerInterceptor(new JoinInterceptor());
        return interceptor;
    }
```

3、使用BasicJoinQueryWrapper (为po advance query预留的，不推荐开发者直接使用)
```java
        BasicJoinQueryWrapper<User> wrapper = new BasicJoinQueryWrapper<>(User.class);
        wrapper.innerJoin(School.class);
        wrapper.eq(new BasicJoinQueryWrapper.ModelProperty(User.class,"schoolId"),1);
        wrapper.select(new BasicJoinQueryWrapper.ModelProperty(User.class,"schoolId"));
        wrapper.select(new BasicJoinQueryWrapper.ModelProperty(User.class,"userId"));
        wrapper.select(new BasicJoinQueryWrapper.ModelProperty(User.class,"name"));
        wrapper.select(new BasicJoinQueryWrapper.ModelProperty(School.class,"schoolName"));
        wrapper.select(new BasicJoinQueryWrapper.ModelProperty(School.class,"id"));
        wrapper.select(new BasicJoinQueryWrapper.ModelProperty(School.class,"remark"));
        mapper.selectList(wrapper);
```
  po advance query写法 ：
  ```java
new User().select(User.SCHOOLID,User.USERID,User.NAME).userId().eq(1).innerJoin(School.class).orgName().like("一").select(School.ID,School.REMARK,School.SCHOOLNAME).list();
```
需要mybatis-plus-advance 依赖：https://gitee.com/baomidou/mybatis-plus-advance

3、使用LambdaJoinQueryWrapper
```java
        // 如果在join后想返回主表就需要 end(主表.class)
        LambdaJoinQueryWrapper<User> wrapper = new LambdaJoinQueryWrapper<>(User.class);
        wrapper.eq(User::getSchoolId,1);
        wrapper.innerJoin(School.class).like(School::getSchoolName,"一");
        mapper.selectList(wrapper);
        // 或者
        wrapper.eq(User::getSchoolId,1)
            .innerJoin(School.class,School::getId,User::getSchoolId)
            .like(School::getSchoolName,"一");
        // 或者 这种方式不用end
        wrapper.eq(User::getSchoolId,1)
        .innerJoin(School.class,School::getId,User::getSchoolId,w -> {
            w.like(School::getSchoolName,"一");
        })

```
### join条件扩展
```java
  // 等于 =
  eq(boolean condition, SFunction<T, Object> column, SFunction<J, Object> val)
  // 不等于 &lt;&gt;
  ne(boolean condition, SFunction<T, Object> column, SFunction<J, Object> val)
  // 大于 &gt;
  gt(boolean condition, SFunction<T, Object> column, SFunction<J, Object> val)
  // 大于等于 &gt;=
  ge(boolean condition, SFunction<T, Object> column, SFunction<J, Object> val)
  // 小于 &lt;
  lt(boolean condition, SFunction<T, Object> column, SFunction<J, Object> val)
  // 小于等于 &lt;=
  le(boolean condition, SFunction<T, Object> column, SFunction<J, Object> val)
  // BETWEEN 值1 AND 值2
  between(boolean condition, SFunction<T, Object> column, SFunction<J, Object> val1, SFunction<J2, Object> val2)
  // NOT BETWEEN 值1 AND 值2
  notBetween(boolean condition, SFunction<T, Object> column, SFunction<J, Object> val1, SFunction<J2, Object> val2)

  eq(School::getId,User::getSchoolId)
  生成的SQL就是  school.id = user.school_id 其他同理
```

### 函数支持
改动：在query接口中添加了selectFun方法，支持用户使用函数来查询字段，目前各类wrapper实现均已支持此查询
```java
 Children selectFun(BaseFuncEnum fun,R alias,R column)
```
1、po上添加接收函数返回的字段
```java
@TableField(exist = false,funField = true) //标记数据库不存在，并且是个函数字段
private Long schoolCount;
```
2、各类wrapper使用
```java
// QueryWrapper
new QueryWrapper<User>().eq("school_id",2).select("school_id").selectFun(DefaultFuncEnum.COUNT,
                "schoolCount").groupBy("school_id")

// LambdaQueryWrapper
new LambdaQueryWrapper<User>().eq(User::getSchoolId,2)
                .select(User::getSchoolId).selectFun(DefaultFuncEnum.COUNT,User::getSchoolCount).groupBy(User::getSchoolId)

// QueryChainWrapper
QueryChainWrapper<User> wrapper = new QueryChainWrapper<User>(mapper);
wrapper.eq("school_id",2).select("school_id").selectFun(DefaultFuncEnum.COUNT,
                "schoolCount").groupBy("school_id").one()
// LambdaQueryChainWrapper
LambdaQueryChainWrapper<User> wrapper = new LambdaQueryChainWrapper<User>(mapper);
wrapper.eq(User::getSchoolId,2)              .select(User::getSchoolId).selectFun(DefaultFuncEnum.COUNT,User::getSchoolCount).groupBy(User::getSchoolId).one()

// BasicJoinQueryWrapper
 BasicJoinQueryWrapper<User> wrapper = new BasicJoinQueryWrapper<>(User.class);
wrapper.eq(new BasicJoinQueryWrapper.ModelProperty(User.class,"schoolId"),2);
wrapper.select(new BasicJoinQueryWrapper.ModelProperty(User.class,"schoolId"));
wrapper.selectFun(DefaultFuncEnum.COUNT,new BasicJoinQueryWrapper.ModelProperty(User.class,"schoolCount"));
wrapper.groupBy(new BasicJoinQueryWrapper.ModelProperty(User.class,"schoolId"));

// LambdaJoinQueryWrapper
LambdaJoinQueryWrapper<User> wrapper = new LambdaJoinQueryWrapper<>(User.class);
wrapper.eq(User::getSchoolId,2);
wrapper.select(User::getSchoolId);
wrapper.selectFun(DefaultFuncEnum.COUNT,User::getSchoolCount);
wrapper.groupBy(User::getSchoolId);
```
