/*
 * Copyright (c) 2011-2022, baomidou (jobob@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.baomidou.mybatisplus.extension.conditions.query.interfaces;

import java.util.function.Consumer;

import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.core.toolkit.support.SFunction;
import com.baomidou.mybatisplus.extension.conditions.query.LambdaJoinQueryWrapper;

/**
 * join封装
 *
 * @author mahuibo
 * @since 2022/11/23
 */
public interface LambdaJoinFunc<T> {

    default <X, J> LambdaJoinQueryWrapper<X> innerJoin(Class<X> joinClass, SFunction<X, ?> leftField, SFunction<J, ?> rightField) {
        return join(joinClass, leftField, rightField, Constants.INNER_JOIN);
    }

    @SuppressWarnings("unchecked")
    default <X, J> LambdaJoinQueryWrapper<T> innerJoin(Class<X> joinClass, SFunction<X, ?> leftField, SFunction<J, ?> rightField, Consumer<LambdaJoinQueryWrapper<X>> consumer) {
        LambdaJoinQueryWrapper<X> joinQueryWrapper = join(joinClass, leftField, rightField, Constants.INNER_JOIN);
        consumer.accept(joinQueryWrapper);
        return (LambdaJoinQueryWrapper<T>) joinQueryWrapper;
    }

    default <X, J> LambdaJoinQueryWrapper<X> leftJoin(Class<X> joinClass, SFunction<X, ?> leftField, SFunction<J, ?> rightField) {
        return join(joinClass, leftField, rightField, Constants.LEFT_JOIN);
    }

    @SuppressWarnings("unchecked")
    default <X, J> LambdaJoinQueryWrapper<T> leftJoin(Class<X> joinClass, SFunction<X, ?> leftField, SFunction<J, ?> rightField, Consumer<LambdaJoinQueryWrapper<X>> consumer) {
        LambdaJoinQueryWrapper<X> joinQueryWrapper = join(joinClass, leftField, rightField, Constants.LEFT_JOIN);
        consumer.accept(joinQueryWrapper);
        return (LambdaJoinQueryWrapper<T>) joinQueryWrapper;
    }

    default <X, J> LambdaJoinQueryWrapper<X> rightJoin(Class<X> joinClass, SFunction<X, ?> leftField, SFunction<J, ?> rightField) {
        return join(joinClass, leftField, rightField, Constants.RIGHT_JOIN);
    }

    @SuppressWarnings("unchecked")
    default <X, J> LambdaJoinQueryWrapper<T> rightJoin(Class<X> joinClass, SFunction<X, ?> leftField, SFunction<J, ?> rightField, Consumer<LambdaJoinQueryWrapper<X>> consumer) {
        LambdaJoinQueryWrapper<X> joinQueryWrapper = join(joinClass, leftField, rightField, Constants.RIGHT_JOIN);
        consumer.accept(joinQueryWrapper);
        return (LambdaJoinQueryWrapper<T>) joinQueryWrapper;
    }

    @SuppressWarnings("unchecked")
    default <X> LambdaJoinQueryWrapper<T> innerJoin(Class<X> joinClass, Consumer<LambdaJoinQueryWrapper<X>> consumer) {
        LambdaJoinQueryWrapper<X> joinQueryWrapper = join(joinClass, null, null, Constants.INNER_JOIN);
        consumer.accept(joinQueryWrapper);
        return (LambdaJoinQueryWrapper<T>) joinQueryWrapper;
    }

    default <X> LambdaJoinQueryWrapper<X> innerJoin(Class<X> joinClass) {
        return join(joinClass, null, null, Constants.INNER_JOIN);
    }

    @SuppressWarnings("unchecked")
    default <X> LambdaJoinQueryWrapper<T> leftJoin(Class<X> joinClass, Consumer<LambdaJoinQueryWrapper<X>> consumer) {
        LambdaJoinQueryWrapper<X> joinQueryWrapper = join(joinClass, null, null, Constants.LEFT_JOIN);
        consumer.accept(joinQueryWrapper);
        return (LambdaJoinQueryWrapper<T>) joinQueryWrapper;
    }

    default <X> LambdaJoinQueryWrapper<X> leftJoin(Class<X> joinClass) {
        return join(joinClass, null, null, Constants.LEFT_JOIN);
    }

    @SuppressWarnings("unchecked")
    default <X> LambdaJoinQueryWrapper<T> rightJoin(Class<X> joinClass, Consumer<LambdaJoinQueryWrapper<X>> consumer) {
        LambdaJoinQueryWrapper<X> joinQueryWrapper = join(joinClass, null, null, Constants.RIGHT_JOIN);
        consumer.accept(joinQueryWrapper);
        return (LambdaJoinQueryWrapper<T>) joinQueryWrapper;
    }

    default <X> LambdaJoinQueryWrapper<X> rightJoin(Class<X> joinClass) {
        return join(joinClass, null, null, Constants.RIGHT_JOIN);
    }

    /**
     * join
     * 如果要添加joinClass的where条件,orderby ，groupby，请使用此接口返回值
     *
     * @param joinClass  join的表的类
     * @param leftField  被连接的表关联字段
     * @param rightField 连接表字段
     * @param joinType   join类型 inner join left join right join
     * @return this
     */
    <X, J> LambdaJoinQueryWrapper<X> join(Class<X> joinClass, SFunction<X, ?> leftField, SFunction<J, ?> rightField, String joinType);

}
