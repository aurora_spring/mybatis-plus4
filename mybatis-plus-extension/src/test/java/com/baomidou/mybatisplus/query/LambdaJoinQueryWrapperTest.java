package com.baomidou.mybatisplus.query;

import java.util.List;

import org.apache.ibatis.builder.MapperBuilderAssistant;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import com.baomidou.mybatisplus.annotation.Relation;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.core.MybatisConfiguration;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.TableInfo;
import com.baomidou.mybatisplus.core.metadata.TableInfoHelper;
import com.baomidou.mybatisplus.extension.conditions.query.LambdaJoinQueryWrapper;
import com.baomidou.mybatisplus.extension.toolkit.SqlHelper;

import lombok.Data;

/**
 * @author mahuibo
 * @since 2022/11/24
 */
public class LambdaJoinQueryWrapperTest {

    @Test
    void testLambdaOrderBySqlSegment() {
        TableInfo tableInfo = TableInfoHelper.initTableInfo(new MapperBuilderAssistant(new MybatisConfiguration(), ""), LambdaJoinQueryWrapperTest.Table.class);
        Assertions.assertEquals("xxx", tableInfo.getTableName());
        LambdaJoinQueryWrapper<LambdaJoinQueryWrapperTest.Table> lqw = new LambdaJoinQueryWrapper<>(LambdaJoinQueryWrapperTest.Table.class);

        lqw.select(Table::getName)
            .leftJoin(TableTwo.class, TableTwo::getTableTwoId, Table::getId)
            .select(TableTwo::getName, TableTwo::getTableTwoId)
            .eq(TableTwo::getTableTwoId, 1);

        List<Table> tables = SqlHelper.getMapper(Table.class)
            .selectList(lqw);

        System.out.println(lqw.getSqlSegment());
        System.out.println(lqw.getSqlSelect());
        System.out.println(lqw.getFrom());
    }

    @Data
    @TableName("xxx")
    private static class Table {

        @TableId("`id`")
        private Long id;

        @TableField("`name`")
        private Long name;

        @TableField(value = "table_two_id", target = TableTwo.class)
        private Long tableTwoId;

        //指定关联关系，和目标类  如果是  onetoone 则不需要指定target
        @TableField(relation = Relation.ONE_TO_MANY, target = TableTwo.class)
        List<TableTwo> tableTwos;
    }

    @Data
    @TableName(value = "xxx2", autoResultMap = true)
    private static class TableTwo {

        @TableId("`table_two_id`")
        private Long tableTwoId;

        @TableField("`name`")
        private Long name;
    }
}
